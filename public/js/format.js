function formatarCPF(documento) {

	if (documento.value.length == 11) {
		var v = documento.value;
		documento.value = "";
		var result = '';

		for (var j = 0; j <= 10; j++)
		{
			if (j == 3 || j == 6) {
				result += '.';
			} 
			if (j == 9) {
				result += '-';
			}
			result += v[j];
		}
		documento.value = result;
	}
}

function formatarRG(documento) {

	if (documento.value.length == 9) {
		var v = documento.value;
		documento.value = "";
		var result = '';

		for (var j = 0; j <= 8; j++)
		{
			if (j == 3 || j == 6) {
				result += '.';
			} 
			result += v[j];
		}
		documento.value = result;
	}
}