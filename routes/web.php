<?php

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/login', function() {
	if (!is_null(Request::user())) {
		Auth::logout();
	}
	return view('login');
});

Route::post('/login', 'MyLoginController@login');

Route::get('/logout',
           [ 'uses' => 'MyLoginController@logout',
             'middleware' => 'role',
             'roles' => ['Administrador', 'Analista', 'Crediarista']]);

// Administrador
Route::get('/adm/usuarios',
           [ 'uses' => 'AdministradorController@usuarios',
             'middleware' => 'role',
             'roles' => ['Administrador']]);

Route::get('/adm/usuario/remover/{id}',
           [ 'uses' => 'AdministradorController@delUsuario',
             'middleware' => 'role',
             'roles' => ['Administrador']]);

Route::get('/adm/usuario/novo',
           [ 'uses' => 'AdministradorController@novoUsuario',
             'middleware' => 'role',
             'roles' => ['Administrador']]);

Route::post('/adm/usuario/novo',
            [ 'uses' => 'AdministradorController@criarUsuario',
              'middleware' => 'role',
              'roles' => ['Administrador']]);

Route::get('/adm/solicitacoes',
           [ 'uses' => 'AdministradorController@solicitacoes',
             'middleware' => 'role',
             'roles' => ['Administrador']]);

Route::get('/adm/solicitacao/{id}',
           [ 'uses' => 'AdministradorController@detalharSolicitacao',
             'middleware' => 'role',
             'roles' => ['Administrador']]);

Route::get('/adm/solicitacao/{id}/aceitar',
           [ 'uses' => 'AdministradorController@aceitarSolicitacao',
             'middleware' => 'role',
             'roles' => ['Administrador']]);

Route::get('/adm/solicitacao/{id}/recusar',
           [ 'uses' => 'AdministradorController@recusarSolicitacao',
             'middleware' => 'role',
             'roles' => ['Administrador']]);

// Crediaristas
Route::get('/cred/solicitacoes',
           [ 'uses' => 'SolicitacaoController@solicitacoesCred',
             'middleware' => 'role',
             'roles' => ['Crediarista']]);

Route::get('/cred/solicitacoes/nova/abertura',
           [ 'uses' => 'SolicitacaoController@aberturaForm',
             'middleware' => 'role',
             'roles' => ['Crediarista']]);

Route::post('/cred/solicitacoes/nova/abertura',
            [ 'uses' => 'SolicitacaoController@aberturaCriar',
              'middleware' => 'role',
              'roles' => ['Crediarista']]);

Route::get('/cred/solicitacoes/nova/cliente',
           [ 'uses' => 'SolicitacaoController@clienteForm',
             'middleware' => 'role',
             'roles' => ['Crediarista']]);

Route::post('/cred/solicitacoes/nova/cliente',
            [ 'uses' => 'SolicitacaoController@clienteCriar',
              'middleware' => 'role',
              'roles' => ['Crediarista']]);

// Analistas
Route::get('/analista/solicitacoes',
           [ 'uses' => 'AnalistaController@solicitacoes',
             'middleware' => 'role',
             'roles' => ['Analista']]);

Route::get('/analista/solicitacao/{id}',
           [ 'uses' => 'AnalistaController@detalharSolicitacao',
             'middleware' => 'role',
             'roles' => ['Analista']]);

Route::get('/analista/solicitacao/{id}/recusar',
           [ 'uses' => 'AnalistaController@recusarSolicitacao',
             'middleware' => 'role',
             'roles' => ['Analista']]);

Route::get('/analista/solicitacao/{id}/aceitar',
           [ 'uses' => 'AnalistaController@aceitarSolicitacao',
             'middleware' => 'role',
             'roles' => ['Analista']]);

Route::get('/analista/solicitacao/{id}/administrador',
           [ 'uses' => 'AnalistaController@enviarParaAdministrador',
             'middleware' => 'role',
             'roles' => ['Analista']]);
