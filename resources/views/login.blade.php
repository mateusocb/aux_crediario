<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Auxílio ao Crediário</title>

	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/login.css') }}">
</head>
<body>
	<div class="container">
		<div id="login-div" class="login-form">
			<h1 id="login-h1" class="text-center">
				Login
			</h1>
			<form action="/login" method="post">
				{{ csrf_field() }}
				<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
					<label for="username">Usuário</label>
					<input autofocus required class="form-control" placeholder="Digite o seu usuario..." type="text" name="username" />
					@if ($errors->has('username'))
					<span class="help-block">
						<strong>{{ $errors->first('username') }}</strong>
					</span>
					@endif
				</div>
				<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
					<label for="password">Senha</label>
					<input required class="form-control" placeholder="Digite a sua senha..." type="password" name="password" />				
					@if ($errors->has('password'))
					<span class="help-block">
						<strong>{{ $errors->first('password') }}</strong>
					</span>
					@endif
				</div>

				<button id="acessar" type="submit" class="btn btn-success col-md-4 col-md-offset-4">
					Acessar
				</button>
			</form>
		</div>
	</div>
</body>
</html>