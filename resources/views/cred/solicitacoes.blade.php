@extends('layouts.user')

@include('cred.menu')

@section('conteudo')

<h1 class="text-center">Gerenciar Solicitações de Crédito</h1>
<hr>
<br>
<div class="container col-md-2">
<a href="/cred/solicitacoes/nova/abertura" style="margin-bottom: 10px;" class="btn btn-success">Abertura de Crédito</a>
<a href="/cred/solicitacoes/nova/cliente" class="btn btn-success">Cliente</a>
</div>
<div class="row col-md-6 col-md-offset-1">
<table class="table table-bordered" style="width: 100%">
	<thead>
	<tr>
		<th class="th">Nome do Cliente</th>
		<th class="th">Nome do Produto</th>
		<th class="th">Plano</th>
		<th class="th">Situação</th>
		<th class="th">Analista</th>
	</tr>
	</thead>
	<tbody>
@foreach ($solicitacoes as $solicitacao)
	<tr class="{{ $solicitacao->situacao == 'pendente' ? 'danger':''}}">
		<td>{{ $solicitacao->nome_cliente }}</td>
		<td>{{ $solicitacao->nome_produto }}</td>
		<td>{{ $solicitacao->plano }}</td>
		<td>{{ $solicitacao->situacao }}</td>
		<td>{{ $solicitacao->analista()->name }}</td>
	</tr>
	@endforeach
	</tbody>
	<tfoot>
		<tr style="height: 10px;">
			<td colspan="5" class="text-center">{{ $links }}</td>
		</tr>
	</tfoot>

</table>
</div>
@endsection