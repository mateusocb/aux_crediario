@extends('layouts.user')

@include('cred.menu')

@section('conteudo')
<h1 class="text-center">Nova Solicitação: Abertura de Crédito</h1>
<hr>
<br>
<div class="container col-md-4 col-md-offset-4">
	<form action="/cred/solicitacoes/nova/abertura" method="post">
		{{ csrf_field() }}
		<fieldset class="form-group">
			<legend>Informações do Cliente:</legend>

			<div class="form-group">
				<label for="nome_cliente">Nome do Cliente</label>
				<input autofocus class="form-control" type="text" name="nome_cliente" required  />
			</div>
			<div class="form-group">
				<label for="cpf">CPF</label>
				<input class="form-control" type="text" maxlength="14" name="cpf" onblur="formatarCPF(this)" />
			</div>
			<div class="form-group">
				<label for="rg">RG</label>
				<input class="form-control" type="text" name="rg" maxlength="11" onblur="formatarRG(this)" />
			</div>
		</fieldset>

		<fieldset>
			<legend>Informações da Solicitação:</legend>
			<div class="form-group">
				<label for="plano">Plano</label>
				<input class="form-control" type="text" name="plano" />
			</div>
			<div class="form-group">
				<label for="nome_produto">Nome do Produto</label>
				<input class="form-control" type="text" name="nome_produto" />
			</div>
			<div class="form-group">
				<label for="observacao">Observações</label>
				<textarea class="form-control" name="observacao"></textarea>
			</div>
		</fieldset>
		<button type="submit" class="btn btn-success">Enviar</button>
	</form>
</div>

@endsection