@extends('layouts.user')

@include('adm.menu')

@section('conteudo')

<h1 class="text-center">Visualizar Solicitação</h1>
<hr>
<br>

<p class="campos">Crediarista: {{ $solicitacao->crediarista()->name }}</p>
<p class="campos">Analista: {{ $solicitacao->analista()->name }}</p>
<p class="campos">Cliente: {{ $solicitacao->nome_cliente }}</p>
<p class="campos">Cpf: {{ $solicitacao->cpfFormatado() }}</p>
<p class="campos">RG: {{ $solicitacao->rgFormatado() }}</p>
<p class="campos">Produto: {{ $solicitacao->nome_produto }}</p>
<p class="campos">Plano: {{ $solicitacao->plano }}</p>
<p class="campos">Situação: {{ $solicitacao->situacao }}</p>
<p class="campos">Tipo: {{ $solicitacao->tipo_solicitacao }}</p>
<p class="campos">Observação: {{ $solicitacao->observacao }}</p>

<a class="btn btn-danger" href="/adm/solicitacao/{{$solicitacao->id}}/recusar">
    Recusar
</a>
<a class="btn btn-success" href="/adm/solicitacao/{{$solicitacao->id}}/aceitar">
    Aceitar
</a>
@endsection
