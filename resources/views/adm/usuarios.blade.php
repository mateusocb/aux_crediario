@extends('layouts.user')

@include('adm.menu')
@section('conteudo')

<h1 class="text-center">Gerenciar Usuários</h1>
<hr>
<br>

<a class="btn btn-primary" href="/adm/usuario/novo" style="margin-bottom: 10px;">Cadastrar Usuario</a>

<table class="table">
	<thead>
		<tr>
			<th>Nome</th>
			<th>Função</th>
			<th>Deletar</th>
		</tr>
	</thead>
	<tbody>
	@foreach ($usuarios as $u)
		<tr>
			<td> <a href="/usuario/{{ $u->id }}">{{ $u->name }}</a></td>
			<td> {{ $u->roles()->first()->name ?? 'nenhum' }} </td>
			<td class="col-sm-1"> 
				<a href="/adm/usuario/remover/{{$u->id}}" >
					<span class="glyphicon glyphicon-trash text-center"></span>
				</a>
			</td>
		</tr>	
	@endforeach		
	</tbody>
	<tfoot>
		<tr style="height: 10px;">
			<td colspan="10" class="text-center">{{ $links }}</td>
		</tr>
	</tfoot>
</table>
@endsection