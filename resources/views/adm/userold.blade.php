<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Auxílio ao Crediário</title>
	{{-- <link rel="stylesheet" type="text/css" href="css/app.css" /> --}}
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ URL::asset('css/sidebar.css') }}" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<body>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
        	<p id="ola" class="text-center">{{ $usuario->name }}</p>
            <ul class="sidebar-nav">
                <li><a href="/adm/solicitacoes"><span class="glyphicon icones glyphicon-th-list"></span>Solicitações</a></li>
                <li><a href="/adm/usuarios"><span class="glyphicon icones glyphicon-user"></span>Usuários</a></li>
                <li><a href="/adm/dados"><span class="glyphicon icones glyphicon-stats"></span>Dados</a></li>
                <li><a href="/logout"> <span id="logout" class="glyphicon icones glyphicon-off"></span>Sair</a></li>
            </ul>
        </div>

        <!-- Page content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
						@yield('conteudo')
                    </div>
                </div>
            </div>
        </div>

    </div>
</body>
</html>