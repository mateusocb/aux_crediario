{{-- @extends('layouts.user') --}}

@section('menu')
	<li><a href="/adm/solicitacoes"><span class="glyphicon icones glyphicon-th-list"></span>Solicitações</a></li>
	<li><a href="/adm/usuarios"><span class="glyphicon icones glyphicon-user"></span>Usuários</a></li>
	<li><a href="#"><span class="glyphicon icones glyphicon-stats"></span>Dados</a></li>
	<li><a href="/logout"> <span id="logout" class="glyphicon icones glyphicon-off"></span>Sair</a></li>
@endsection