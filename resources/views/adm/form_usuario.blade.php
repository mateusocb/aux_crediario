@extends('layouts.user')

@include('adm.menu')
@section('conteudo')

<h1 class="text-center">Cadastrar Novo Usuário</h1>
<hr>
<br>
<div class="container col-sm-6 col-sm-offset-3">
<form action="/adm/usuario/novo" method="post">
	{{ csrf_field() }}
	
	<div class="form-group">
		<label for="name">Nome:</label>
		<input class="form-control" type="text" name="name">
	</div>

	<div class="form-group">
		<label for="username">Usuário:</label>
		<input class="form-control type="text" name="username">
	</div>

	<div class="form-group">
		<label for="password">Senha:</label>
		<input class="form-control" type="password" name="password">
	</div>

	<div class="form-group">
		<label for="permission">Tipo de Usuário:</label>
		<div class="form-check">
			<label clas="form-check-label">
			<input class="form-check-input" type="radio" name="permissao" value="Crediarista" checked>
				Crediarista
			</label>
			<label class="form-check-label">
				<input class="form-check-input" type="radio" name="permissao" value="Analista">
				Analista
			</label>
			<label class="form-check-label">
				<input class="form-check-input" type="radio" name="permissao" value="Administrador">
				Administrador
			</label>
		</div>
	</div>
	<button class="btn btn-success" type="submit">Cadastrar</button>
</form>
</div>
@endsection