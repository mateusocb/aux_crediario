@extends('layouts.user')

@include('adm.menu')
@section('conteudo')

<h1 class="text-center">Gerenciar Solicitações</h1>
<hr>
<br>

<div class="row col-md-6 col-md-offset-3">

<table class="table" style="width: 100%">
	<thead>
	<tr>
		<th class="th">Nome do Cliente</th>
		<th class="th">Nome do Produto</th>
		<th class="th">Plano</th>
		<th class="th">Situação</th>
		<th class="th">Analista</th>
        <th class="th">Visualizar</th>
    </tr>
    </thead>
	<tbody>
@foreach ($solicitacoes as $solicitacao)
	<tr class="{{ $solicitacao->situacao == 'pendente' ? 'danger':''}}">
		<td>{{ $solicitacao->nome_cliente }}</td>
		<td>{{ $solicitacao->nome_produto }}</td>
		<td>{{ $solicitacao->plano }}</td>
		<td>{{ $solicitacao->situacao }}</td>
		<td>{{ $solicitacao->analista()->name }}</td>
<td>
     <a href="/adm/solicitacao/{{$solicitacao->id}}">
    <span class="glyphicon glyphicon-eye-open"></span>
    </a>
    </td>
    </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr style="height: 10px;">
            <td colspan="5" class="text-center">{{ $links }}</td>
		</tr>
	</tfoot>
</table>
</div>
@endsection