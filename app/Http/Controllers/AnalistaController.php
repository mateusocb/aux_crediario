<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App;
class AnalistaController extends Controller
{
	public function solicitacoes() {
        $solicitacoes = App\Solicitacao::where('cod_analista', Auth::user()->id)
        ->orderBy('updated_at', 'desc')
        ->paginate(10);

		return view('analista.solicitacoes')
		->with(['usuario' => Auth::user(),
			'solicitacoes' => $solicitacoes,
            'links' => $solicitacoes->links()]);
	}

    public function detalharSolicitacao($id) {

      $solicitacao = App\Solicitacao::find($id);

      return view('analista.solicitacao')->with(['usuario' => Auth::user(),
        'solicitacao' => $solicitacao]);
    }

  public function aceitarSolicitacao($id) {
    $solicitacao = App\Solicitacao::find($id);

    if ($solicitacao->situacao == "pendente") {
      $solicitacao->situacao = "analisada";
      $solicitacao->save();
    }

    return redirect('/analista/solicitacoes');
  }

  public function recusarSolicitacao($id) {
    $solicitacao = App\Solicitacao::find($id);

    if ($solicitacao->situacao == "pendente") {
      $solicitacao->situacao = "recusada";
      $solicitacao->save();
    }

    return redirect('/analista/solicitacoes');
  }

  public function enviarParaAdministrador($id) {
    $solicitacao = App\Solicitacao::find($id);

    if ($solicitacao->situacao == "pendente") {
      $solicitacao->situacao = "analise_adm";
      $solicitacao->save();
    }

    return redirect('/analista/solicitacoes');
  }

}
