<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App;

class SolicitacaoController extends Controller
{

    public function solicitacoesCred() {
        $solicitacoes = App\Solicitacao::where('cod_crediarista', Auth::user()->id)
                      ->orderBy('updated_at', 'desc')
                      ->paginate(10);

        return view('cred.solicitacoes')
            ->with(['usuario' => Auth::user(),
                    'solicitacoes' => $solicitacoes,
                    'links' => $solicitacoes->links()]);
    }
    
    public function aberturaForm() {
        return view('cred.form_abertura')
            ->with(['usuario' => Auth::user()]);
    }

    public function clienteForm() {
        return view('cred.form_cliente')
            ->with(['usuario' => Auth::user()]);;
    }

    public function aberturaCriar(Request $request) {
        $cpfNovo = str_replace(['.', '-'], '', $request->cpf);
        $rgNovo = str_replace(['.', '-'], '', $request->rg);
        
        $solicitacao = new App\Solicitacao();
        $solicitacao->cod_crediarista = Auth::user()->id;
        $solicitacao->cod_analista = $this->pegarAnalistaDisponivel();
        $solicitacao->nome_cliente = $request->nome_cliente;
        $solicitacao->cpf = $cpfNovo;
        $solicitacao->rg = $rgNovo;
        $solicitacao->plano = $request->plano;
        $solicitacao->nome_produto = $request->nome_produto;
        $solicitacao->tipo_solicitacao = 'abertura';
        $solicitacao->observacao = $request->observacao;
        $solicitacao->save();
        return redirect('/cred/solicitacoes');
    }

    public function clienteCriar() {
        $cpfNovo = str_replace(['.', '-'], '', $request->cpf);
        $rgNovo = str_replace(['.', '-'], '', $request->rg);
        
        $solicitacao = new App\Solicitacao();
        $solicitacao->cod_crediarista = Auth::user()->id;
        $solicitacao->cod_analista = $this->pegarAnalistaDisponivel();
        $solicitacao->nome_cliente = $request->nome_cliente;
        $solicitacao->cpf = $cpfNovo;
        $solicitacao->rg = $rgNovo;
        $solicitacao->plano = $request->plano;
        $solicitacao->nome_produto = $request->nome_produto;
        $solicitacao->tipo_solicitacao = 'cliente';
        $solicitacao->observacao = $request->observacao;
        $solicitacao->save();
        return redirect('/cred/solicitacoes');
    }

    public function pegarAnalistaDisponivel() {
        $usuarios = App\User::all();
        $listaIds = array();
      
        foreach($usuarios as $u) {
            if ($u->roles()->first()->name == "Analista") {
                array_push($listaIds, $u->id);
            }
        }

        shuffle($listaIds);
      
        return $listaIds[0];
    }
}
