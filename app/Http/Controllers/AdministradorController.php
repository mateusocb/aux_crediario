<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AdministradorController extends Controller
{

	public function usuarios() {
		$usuarios = \App\User::orderBy('updated_at', 'desc')
        			->paginate(10);
		return view('adm.usuarios')
					->with([
						'usuario' => Auth::user(),
						'usuarios' => $usuarios,
						'links' => $usuarios->links()
						]);
	}

	public function novoUsuario() {

		return view('adm.form_usuario')->with([ 'usuario' => Auth::user() ]);
	}

	public function criarUsuario(Request $request) {
		
		$role= \App\Role::where('name', $request->permissao)->first();
		$user = new \App\User();

		$user->name = $request->name;
		$user->username = $request->username;
		$user->password = bcrypt($request->password);
		$user->save();

		$user->roles()->attach($role);

		return redirect('/adm/usuarios');
	}

	public function delUsuario($id) {

		$user = \App\User::find($id);

		$user->delete();

        return redirect('/adm/usuarios');
    }


    public function solicitacoes() {
        $solicitacoes = \App\Solicitacao::where('situacao', 'analise_adm')
                      ->orderBy('updated_at', 'desc')
                      ->paginate(10);

        return view('adm.solicitacoes')
                      ->with(['usuario' => Auth::user(),
                              'solicitacoes' => $solicitacoes,
                              'links' => $solicitacoes->links()]);
    }

    public function detalharSolicitacao($id) {
        $solicitacao = \App\Solicitacao::find($id);
       
        return view('adm.solicitacao')
            ->with(['usuario' => Auth::user(),
                    'solicitacao' => $solicitacao]);
    }

  public function aceitarSolicitacao($id) {
    $solicitacao = \App\Solicitacao::find($id);

    if ($solicitacao->situacao == "analise_adm") {
      $solicitacao->situacao = "analisada";
      $solicitacao->save();
    }

    return redirect('/adm/solicitacoes');
  }

  public function recusarSolicitacao($id) {
    $solicitacao = \App\Solicitacao::find($id);

    if ($solicitacao->situacao == "analise_adm") {
      $solicitacao->situacao = "recusada";
      $solicitacao->save();
    }

    return redirect('/adm/solicitacoes');
  }
}
