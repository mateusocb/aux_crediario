<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitacao extends Model
{
	protected $fillable = [
		'nome_cliente',
		'cpf',
		'rg',
		'plano',
		'nome_produto',
		'observacao'
	];
	
    public function crediarista() {
    	return \App\User::find($this->cod_crediarista);
    }

    public function analista() {
    	return \App\User::find($this->cod_analista);
    }

  public function cpfFormatado() {
    
    $parte_um     = substr($this->cpf, 0, 3);
    $parte_dois   = substr($this->cpf, 3, 3);
    $parte_tres   = substr($this->cpf, 6, 3);
    $parte_quatro = substr($this->cpf, 9, 2);

    return "$parte_um.$parte_dois.$parte_tres-$parte_quatro";
    
  }

  public function rgFormatado() {

    $parte_um     = substr($this->rg, 0, 3);
    $parte_dois   = substr($this->rg, 3, 3);
    $parte_tres   = substr($this->rg, 6, 3);

    return "$parte_um.$parte_dois.$parte_tres";
  }
}
