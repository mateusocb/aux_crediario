<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$role_crediarista = Role:: where('name', 'Crediarista')->first();
    	$role_analista = Role:: where('name', 'Analista')->first();
    	$role_administrador = Role:: where('name', 'Administrador')->first();

    	$crediarista = new User();
    	$crediarista->name = 'Patricia Souza';
    	$crediarista->username = 'crediarista1';
    	$crediarista->password = bcrypt('123456');
    	$crediarista->save();
    	$crediarista->roles()->attach($role_crediarista);

        $crediarista = new User();
        $crediarista->name = 'Carla Fernandes';
        $crediarista->username = 'crediarista2';
        $crediarista->password = bcrypt('123456');
        $crediarista->save();
        $crediarista->roles()->attach($role_crediarista);

    	$analista = new User();
    	$analista->name = 'Joyce Lima';
    	$analista->username = 'analista1';
    	$analista->password = bcrypt('123456');
    	$analista->save();
    	$analista->roles()->attach($role_analista);

        $analista = new User();
        $analista->name = 'Karla Perez';
        $analista->username = 'analista';
        $analista->password = bcrypt('123456');
        $analista->save();
        $analista->roles()->attach($role_analista);

        $administrador = new User();
        $administrador->name = 'Mateus Oliveira';
        $administrador->username = 'adm1';
        $administrador->password = bcrypt('123456');
        $administrador->save();
        $administrador->roles()->attach($role_administrador);
		
        $administrador = new User();
    	$administrador->name = 'Thiago Avelino';
    	$administrador->username = 'adm2';
        $administrador->password = bcrypt('123456');
        $administrador->save();
    	$administrador->roles()->attach($role_administrador);

    }
}
