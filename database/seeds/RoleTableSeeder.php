<?php

use Illuminate\Database\Seeder;
use App\Role;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$role_crediarista = new Role();
    	$role_crediarista->name = "Crediarista";
    	$role_crediarista->description = "Essa role é responsável pelos crediaristas.";
    	$role_crediarista->save();

    	$role_analista = new Role();
    	$role_analista->name = "Analista";
    	$role_analista->description = "Essa role é responsável pelos analistas.";
    	$role_analista->save();

    	$role_administrador = new Role();
    	$role_administrador->name = "Administrador";
    	$role_administrador->description = "Essa role é responsável pelos administradores.";
    	$role_administrador->save();

    }
}
