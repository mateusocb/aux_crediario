<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitacaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitacaos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cod_crediarista');
            $table->integer('cod_analista');
            $table->string('cod_cliente')->nullable(true);
            $table->string('nome_cliente');
            $table->string('cpf', 11)->nullable(true);
            $table->string('rg', 9)->nullable(true);
            $table->string('plano');
            $table->string('nome_produto');
            $table->string('cod_solicitacao')->nullable(true);
            $table->string('situacao', 15)->default('pendente');
            $table->string('tipo_solicitacao', 10);
            $table->text('observacao')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitacaos');
    }
}
